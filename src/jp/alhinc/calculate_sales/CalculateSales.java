package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";
	private static final String BRANCH_NAME = "支店定義ファイル";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";
	private static final String COMMODITY_NAME = "商品定義ファイル";

	// 正規表現
	private static final String BRANCH_CODE = "^[0-9]{3}$";
	private static final String SALES_FILE_NAME_CODE = "^[0-9]{8}.rcd$";
	private static final String COMMODITY_CODE = "^[A-Za-z0-9]{8}$";
	private static final String NUMBERS_ONLY = "^[0-9]+$";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String FILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String FILE_NAMES_NOT_SERIAL = "売上ファイル名が連番になっていません";
	private static final String SALES_FILE_INVALID_FORMAT = "の支店コードが不正です";
	private static final String TOTAL_OVER_ONE_BILLION = "合計金額が10桁を超えました";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, BRANCH_NAME, branchNames, branchSales, BRANCH_CODE, FILE_NOT_EXIST,
				FILE_INVALID_FORMAT)) {
			return;
		}
		// 商品定義ファイルの読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, COMMODITY_NAME, commodityNames, commoditySales, COMMODITY_CODE,
				FILE_NOT_EXIST, FILE_INVALID_FORMAT)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			if ((files[i].isFile()) && (files[i].getName().matches(SALES_FILE_NAME_CODE))) {
				rcdFiles.add(files[i]);
			}
		}

		Collections.sort(rcdFiles);
		for (int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(FILE_NAMES_NOT_SERIAL);
				return;
			}
		}

		BufferedReader br = null;
		for (int i = 0; i < rcdFiles.size(); i++) {
			List<String> items = new ArrayList<>();

			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(fr);

				String line;
				while ((line = br.readLine()) != null) {
					items.add(line);
				}

				String getName = rcdFiles.get(i).getName();

				if (items.size() != 3) {
					System.out.println(getName + SALES_FILE_INVALID_FORMAT);
					return;
				}

				if (!branchNames.containsKey(items.get(0))) {
					System.out.println(getName + SALES_FILE_INVALID_FORMAT);
					return;
				}

				if (!items.get(2).matches(NUMBERS_ONLY)) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				Long fileSales = Long.parseLong(items.get(2));
				// 支店別合計金額計算
				Long salesAmount = branchSales.get(items.get(0)) + fileSales;
				// 商品別合計金額計算
				Long commodityAmount = commoditySales.get(items.get(1)) + fileSales;

				if (salesAmount >= 10000000000L) {
					System.out.println(TOTAL_OVER_ONE_BILLION);
					return;
				}
				if (commodityAmount >= 10000000000L) {
					System.out.println(TOTAL_OVER_ONE_BILLION);
					return;
				}

				branchSales.put(items.get(0), salesAmount);
				commoditySales.put(items.get(1), commodityAmount);

			} catch (Exception e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (Exception e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName,String fileType, Map<String, String> namesMap,
			Map<String, Long> salesMap, String codeCheck, String unexistError, String formatError) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			if (!file.exists()) {
				System.out.println(fileType  + unexistError);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				if ((items.length != 2) || (!items[0].matches(codeCheck))) {
					System.out.println(fileType + formatError);
					return false;
				}

				namesMap.put(items[0], items[1]);
				salesMap.put(items[0], 0L);

			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> namesMap,
			Map<String, Long> salesMap) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)

		BufferedWriter bw = null;
		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (String key : namesMap.keySet()) {
				bw.write(key + "," + namesMap.get(key) + "," + salesMap.get(key));
				bw.newLine();
			}
		} catch (Exception e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}

		return true;
	}

}
